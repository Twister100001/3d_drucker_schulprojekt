# 3D-Drucker_Schulprojekt


## 3D Drucker Anforderung

Der Kunde Flashforge will für sein neues 3D-Drucker Modell eine Steuerungssoftware bzw. Firmware bestellen.
Der Drucker soll Dateien im G-File und GX-File Format unterstützen und ausdrucken können. Falls ein anderes Dateiformat zum Druck aufgegeben wird, soll dem Benutzer eine entsprechende Fehlermeldung angezeigt werden. Diese Files können mit einem USB Stick auf dem Drucker übertragen werden.
Dazu kann der Benutzer im Menu auf «FILES SUCHEN» drücken und es werden ihm die Inhalte des USB-Sticks angezeigt. Der Benutzer soll dann ein File auswählen, welches in den Speicher des Druckers geladen und direkt ausgedruckt wird.
Weiterhin sollen die Benutzer zu Testzwecken den Drucker vorheizen können. Hier soll im Menu eine Option «VORHEITZEN» gebraucht werden. Nach der Auswahl soll die Firmware den Benutzer nach der gewünschten Temperatur fragen und die Temperatur des Druckkopfes in 1° Schritte erhöhen.
Für den täglichen Gebrauch, soll der Drucker auch die Option bieten, die Filament-Spule auszuwechseln. Dazu wird der Benutzer aufgefordert den Deckel zu öffnen und diese Aktion mit einem OK zu bestätigen. Danach heizt sich der Druckkopf auf 240° (diesmal in 5° Schritte) auf. Sobald die Temperatur auf 240° ist beginnt die Spule gegen den Uhrzeigersinn zu drehen und zieht so das Filament raus. Dies dauert in der Regel 3-5 Sekunden.
Nach dem Ausziehen zeigt der Drucker den Benutzer auf, das die Spule entfernt und eine neue eingesetzt werden kann. Der Benutzer muss dies wieder mit einem OK bestätigen. Danach wird die Spule vom Drucker im Uhrzeigersinn gedreht und lädt somit das Filament in den Kopf. Zu guter letzt kühlt sich der Kopf auf und der Drucker gibt dem Benutzer an, das er fertig ist.
Für das Drucken selbst, soll zuerst ein G- bzw. GX-File eingelesen werden. Dieses File beinhaltet Informationen wie etwas gedruckt werden soll, wie heiss die Temperatur eingestellt ist und wie lange der Druck dauern wird.
Nach dem Einlesen wird der Kopf auf die eingestellte Temperatur in 5° Schritte erhöht und gibt die geschätzte Zeit für den Druck aus. Danach beginnt der Drucker mit dem Drucken des Fileinhaltes.
Dazu zieht er wieder Filament ein, indem er die Spule im Uhrzeigersinn dreht und durch den erhitzten Kopf zieht.
Zu guter Letzt wird dem Benutzer noch die Meldung angezeigt, das der Druck fertig ist (sobald der Druck fertig ist).

***

## ToDo

- [x] NSD für Projekt
- [x] Gitlab, Git undCLion aufsetzen
- [x] Gitlab Projekt für P3d drucker erstellen, NSD hochladen
- [ ] Struktogram für Gruppen-Projekt erstellen (Hauptablauf)
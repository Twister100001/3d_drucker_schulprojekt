//
// Created by mikaj on 12.04.2023.
//

#include "stdio.h"
#include "unistd.h"
#include "string.h"
#include "lib/File_suchen.h"
#include "lib/Vorheizen.h"
#include "lib/Filament.h"
#include "lib/Drucken.h"
#include "lib/file_struct.h"

unsigned int tempist = 20;
struct  DruckDatei printingfile;


int main(){

    int Menu;
    while(1){

        unsigned int tempsoll = 0;
        unsigned int schritt = 1;

        printf("Menu: \n");
        printf("1: File suchen \n");
        printf("2: Vorheizen \n");
        printf("3: Filament \n");
        printf("4: Drucken \n");
        printf("5: Ausschalten \n\n");


        if(1==scanf("\n%d",&Menu)){}

        else {
            printf("Es koennen nur Zahlen akzeptiert werden. \n"
                   "Programm wird heruntergefahren... \n");
            sleep(1);
           return 0;
        }

        switch(Menu) {
            case 1:
                printf("File suchen: \n");
                printingfile = filesuchen();
                sleep(1);

                break;

            case 2:

                printf("Vorheizen: \n");
                printf("Bitte gebe die Wunschtemparatur ein:\n");
                scanf("%d", &tempsoll);
                tempist = vorheizen(tempsoll, schritt, tempist);
                sleep(1);

                break;

            case 3:
                printf("Filament: \n");
                sleep(1);
                tempist = filament(tempist);

                break;

            case 4:
                printf("Drucken: \n");
                int dauer;
                dauer = printingfile.dauer;
                int temperatur;
                temperatur = printingfile.temperatur;
                int length;
                length = printingfile.length;
                char content [200][200];
                memcpy(content,printingfile.content,200*200);

                tempist = drucken(tempist, dauer, temperatur, length, content);
                printingfile.length = 0;
                sleep(1);
                break;

            case 5:
                printf("Wird heruntergefahren... \n");
                sleep(1);
                return 0;


            default:
                printf("Ungueltige Eingabe. Bitte erneut versuchen. \n");

                Menu = 0;
                break;
        }
    }
}

//
// Created by mikaj on 12.04.2023.
//

#include "Filament.h"
#include "Vorheizen.h"
#include "stdio.h"
#include "unistd.h"

// Funktion zur Filamentverarbeitung
unsigned int filament(unsigned int tempist) {
    int tempsoll = 240; // Solltemperatur
    int schritt = 5; // Temperaturschritt
    char offen; // Variable fuer den Deckelstatus
    char spule; // Variable fuer den Spulenstatus
    char zu; // Variable fuer den Deckelstatus

    // Ueberpruefung des Deckelstatus
    status_offen:
    printf("Bitte Deckel oeffnen! \n");
    printf("Ist der Deckel geoeffnet? \n");
    printf("'j/n' \n");
    scanf(" %c", &offen);

    switch (offen) {
        case 'j':
            // Vorheizen auf die gewuenschte Temperatur
            tempist = vorheizen(tempsoll, schritt, tempist);
            printf("'Spule dreht sich im Gegenuhrzeigersinn' \n");
            printf("Filament wird ausgegeben. \n");
            sleep(4);
            break;

        case 'n':
            sleep(1);
            goto status_offen;

        default:
            printf("Fehler! \n"
                   "Bitte 'j' oder 'n' eingeben. \n");
            sleep(1);
            goto status_offen;
    }

    printf("Spule kann ersetzt werden. \n");

    // Ueberpruefung des Spulenstatus
    status_spule:
    printf("Ist die neue Spule eingesetzt? \n");
    printf("'j/n' \n");
    scanf(" %c", &spule);

    switch (spule) {
        case 'j':
            printf("'Spule dreht sich im Uhrzeigersinn' \n");
            sleep(2);
            printf("Bitte Deckel schliessen. \n");

            // Ueberpruefung des Deckelstatus nach dem Schliessen
        status_zu:
            printf("Ist der Deckel geschlossen? (j/n) \n");
            printf("'j/n' \n");
            scanf(" %c", &zu);
            break;

        case 'n':
            printf("Bitte Spule einsetzen. \n");
            sleep(1);
            goto status_spule;

        default:
            printf("Fehler! \n"
                   "Bitte 'j' oder 'n' eingeben. \n");
            sleep(1);
            goto status_spule;
    }

    switch (zu) {
        case 'j':
            tempsoll = 60;
            printf("Spule gewechselt. \n");
            printf("Wird abgekuehlt. \n");
            tempist = vorheizen(tempsoll, schritt, tempist);
            break;

        case 'n':
            printf("Bitte Deckel schliessen! \n");
            sleep(1);
            goto status_zu;

        default:
            printf("Fehler! \n"
                   "Bitte 'j' oder 'n' eingeben. \n");
            sleep(1);
            goto status_zu;
    }

    return tempist;
}
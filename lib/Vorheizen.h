//
// Created by mikaj on 12.04.2023.
//

#ifndef INC_3D_DRUCKER_SCHULPROJEKT_VORHEIZEN_H
#define INC_3D_DRUCKER_SCHULPROJEKT_VORHEIZEN_H

unsigned int vorheizen(unsigned int tempsoll, unsigned int schritt, unsigned int tempist);

#endif //INC_3D_DRUCKER_SCHULPROJEKT_VORHEIZEN_H
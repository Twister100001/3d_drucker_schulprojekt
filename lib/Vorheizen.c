//
// Created by mikaj on 12.04.2023.
//

#include "Vorheizen.h"
#include "stdio.h"
#include "unistd.h"

// Funktion zum Vorheizen der Temperatur
unsigned int vorheizen(unsigned int tempsoll, unsigned int schritt, unsigned int tempist) {
    int timer = 500000;

    // Solange die gewuenschte Temperatur noch nicht erreicht ist
    while (tempsoll != tempist) {
        // Temperaturdifferenz zwischen Soll- und Ist-Temperatur berechnen
        unsigned int tempdiff = tempsoll - tempist;

        // Falls der Schritt kleiner als die Temperaturdifferenz ist
        if (schritt < tempdiff) {
            if (tempsoll > tempist) {
                // Temperatur erhoehen
                tempist = tempist + schritt;
                usleep(timer); // Verzoegerung fuer den Schritt
            } else {
                // Temperatur verringern
                tempist = tempist - schritt;
                usleep(timer); // Verzoegerung fuer den Schritt
            }
        } else {
            // Wenn der Schritt groesser oder gleich der Temperaturdifferenz ist,
            // die Temperatur auf den Sollwert setzen
            tempist = tempsoll;
        }

        printf("Die Temperatur ist nun %d Grad Celsius. \n", tempist);
    }

    printf("Die Temperatur von %d Grad ist eingestellt. \n", tempist);
    return tempist;
}
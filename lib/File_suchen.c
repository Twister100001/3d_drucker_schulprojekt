//
// Created by mikaj on 12.04.2023.
//

#include "File_suchen.h"
#include "file_struct.h"
#include "stdio.h"
#include "dirent.h"
#include "string.h"
#include "ctype.h"
#include "stdlib.h"

const char fileformat[][4] = {".G", ".g", ".Gx", ".gx", ".gX", ".GX"};
const int fileformatCount = sizeof(fileformat) / sizeof(fileformat[0]);

// Funktion zum Auflisten des Verzeichnisses
void list_directory() {
    int j = 1;
    struct dirent *de;
    DIR *dr = opendir("./druckfiles");

    if (dr == NULL) {
        printf("Aktuelles Verzeichnis konnte nicht geoeffnet werden.\n");
        return;
    }

    while ((de = readdir(dr)) != NULL) {
        char *filename = de->d_name;
        size_t length = strlen(filename);

        for (int i = 0; i < fileformatCount; i++) {
            size_t lengthsuffix = strlen(fileformat[i]);

            if (lengthsuffix <= length) {
                int compare_result = strcmp(filename + (length - lengthsuffix), fileformat[i]);
                if (compare_result == 0) {
                    printf("%d: %s\n", j, filename);
                    j++;
                    break;
                }
            }
        }
    }

    closedir(dr);
}

// Funktion zum ueberpruefen der Dateierweiterung
int verify_file(const char *file) {
    int length = strlen(file);

    for (int i = 0; i < fileformatCount; i++) {
        size_t lengthsuffix = strlen(fileformat[i]);

        if (lengthsuffix <= length) {
            int compare_result = strcmp(file + (length - lengthsuffix), fileformat[i]);
            if (compare_result == 0) {
                return 1;
            }
        }
    }

    return 0;
}

// Funktion zum Suchen der Druckdatei
struct DruckDatei filesuchen(void) {
    int row = 0;
    char druckfile[260];
    char folder[260] = "./druckfiles/";

    do {
        list_directory();
        printf("Geben Sie den Namen der zu druckenden Datei ein oder waehlen Sie eine Nummer aus der Liste aus: ");
        scanf("%s", druckfile);
        printf("Die Eingabe war: %s\n", druckfile);

        if (verify_file(druckfile)) {
            break;
        } else if (isdigit(druckfile[0])) {
            int selected_number = atoi(druckfile);
            int count = 0;
            struct dirent *de;
            DIR *dr = opendir("./druckfiles");

            if (dr == NULL) {
                printf("Aktuelles Verzeichnis konnte nicht geoeffnet werden.\n");
                struct DruckDatei emptyPrintFile = {0};
                return emptyPrintFile;
            }

            while ((de = readdir(dr)) != NULL) {
                char *filename = de->d_name;
                size_t length = strlen(filename);

                for (int i = 0; i < fileformatCount; i++) {
                    size_t lengthsuffix = strlen(fileformat[i]);

                    if (lengthsuffix <= length) {
                        int compare_result = strcmp(filename + (length - lengthsuffix), fileformat[i]);
                        if (compare_result == 0) {
                            count++;
                            if (count == selected_number) {
                                strcpy(druckfile, filename);
                                break;
                            }
                        }
                    }
                }
                if (count == selected_number) {
                    break;
                }
            }

            closedir(dr);

            if (count != selected_number) {
                printf("Ungueltige Dateinummer. Bitte versuchen Sie es erneut.\n");
                continue;
            }
            break;
        } else {
            printf("Ungueltige Eingabe. Bitte versuchen Sie es erneut.\n");
        }
    } while (1);

    printf("Die Datei %s ist gueltig.\n", druckfile);       //Geht nur um das Lesen des Files. Es geht nicht um dessen Inhalt
    strcat(folder, druckfile);

    FILE *p_file = fopen(folder, "r");
    if (p_file == NULL) {
        printf("Die Datei konnte nicht geoeffnet werden.\n");
        struct DruckDatei emptyPrintFile = {0};
        return emptyPrintFile;
    } else {
        struct DruckDatei printfile = {0};
        char line[200];
        fgets(line, sizeof(line), p_file);
        sscanf(line, "%d;%d", &printfile.dauer, &printfile.temperatur);

        while (fgets(line, sizeof(line), p_file)) {
            strcpy(printfile.content[row], line);
            row++;
        }

        fclose(p_file);

        printfile.length = row;
        return printfile;
    }
}
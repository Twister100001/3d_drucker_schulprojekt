//
// Created by mikaj on 12.04.2023.
//

#include "Drucken.h"
#include "Vorheizen.h"
#include "File_suchen.h"
#include "file_struct.h"
#include "stdio.h"
#include "unistd.h"
#include "string.h"

 struct DruckDatei printfile;

int drucken(int tempist,int dauer,  int temperatur,  int length,  char content [200][200]) {

    printfile.dauer = dauer;
    printfile.temperatur = temperatur;
    printfile.length = length;
    memcpy(printfile.content, content, 200*200);

    int validfile = 1;
    while (validfile == 1) {
        if (printfile.length == 0) {

            printf("Leeres File. Bitte eine andere Datei auswaehlen.\n");
            printfile = filesuchen(); // Die Funktion filesuchen() wird aufgerufen, um die printfile-Struktur zu erhalten
        } else if (printfile.dauer == 0) {

            printf("Fehlerhaftes File. Bitte eine andere Datei auswaehlen.\n");
            printfile = filesuchen(); // Die Funktion filesuchen() wird aufgerufen, um die printfile-Struktur zu erhalten

        } else if (printfile.temperatur == 0) {

            printf("Fehlerhaftes File. Bitte eine andere Datei auswaehlen.\n");
            printfile = filesuchen(); // Die Funktion filesuchen() wird aufgerufen, um die printfile-Struktur zu erhalten

        }else {validfile = 0;}
    }
    int tempsoll = printfile.temperatur; // Die Temperatur wird aus der printfile-Struktur abgerufen
    int time = printfile.dauer; // Die Zeit wird aus der printfile-Struktur abgerufen
    int total_characters = 0;
    int total_time = 0;
    int tempcheck = printfile.temperatur;

    if(tempcheck == 0){

        tempsoll = tempist;


    }

    for (int i = 0; i < printfile.length; i++) {
        total_characters += strlen(printfile.content[i]);
    }

    if (total_characters == 0) {
        printf("Datei ist leer. Es gibt nichts zu drucken.\n");
        return tempist;
    }

    tempist = vorheizen(tempsoll, 5, tempist);
    int time_per_character = time / total_characters; // Die Zeit pro Zeichen wird berechnet

    printf("Geschaetzte Druckzeit: %d Sekunden\n", time/1000);

    for (int i = 0; i < printfile.length; i++) {
        char* line = printfile.content[i];
        unsigned long length = strlen(line);

        for (int j = 0; j < length; j++) {
            printf("%c", line[j]);
            fflush(stdout);
            usleep(time_per_character*1000); // Verzoegerung fuer die berechnete Zeit pro Zeichen in Millisekunden
            total_time += time_per_character; // Die Gesamtzeit wird inkrementiert
        }
    }

    printf("\n");
    printf("Gesamte Druckzeit: %d Sekunden\n", total_time/1000);
    return tempist;
}
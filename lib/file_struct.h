//
// Created by mikaj on 24.05.2023.
//

#ifndef INC_3D_DRUCKER_SCHULPROJEKT_FILE_STRUCT_H
#define INC_3D_DRUCKER_SCHULPROJEKT_FILE_STRUCT_H

struct DruckDatei {
    int dauer;
    int temperatur;
    int length;
    char content [200][200];
};

#endif //INC_3D_DRUCKER_SCHULPROJEKT_FILE_STRUCT_H